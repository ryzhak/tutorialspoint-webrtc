//init fabric.js
//var canvas = new fabric.Canvas('canvas');
//canvas.freeDrawingBrush.width = 5;

/*
*  Fabric handlers
*/

// create a rectangle object
/*var btnAddRect = document.getElementById('btnAddRect');
btnAddRect.onclick = function(e){
	e.preventDefault();
	canvas.isDrawingMode = false;
	var rect = new fabric.Rect({
	  left: 100,
	  top: 100,
	  fill: 'green',
	  width: 100,
	  height: 100
	});
	// "add" rectangle onto canvas
	canvas.add(rect);
};

//enter brush mode
var btnBrush = document.getElementById('btnBrush');
btnBrush.onclick = function(e){
	e.preventDefault();
	canvas.isDrawingMode = true;
};*/

/*
*  End fabric handlers
*/

/*
*  Initial variables for webrtc block
*/

//modal window with a link to share
var modalShareLink = $("#modalShareLink");
//room name created on the server
var roomName;
//a place where a shared link will be displayed
var textShareLink = $("#textShareLink");
//part of the screen that admin will share with users
var partOfScreenToBeShared = document.getElementById('canvasBoard');

//init context menu on videos
webrtcPanel.contextmenu({
	target:'#menuAdminContext', 
	// execute code before context menu if shown
	before: function(e,context) {
    	//if admin clicks on his own video or his own image then disable it
    	var clickedUserId = $(context).closest('div').data('userId');
    	if(connection.userid === clickedUserId){
    		return false;
    	}
    	return true;
	},
	// execute on menu item selection
	onItem: function(context,e) {

    	//clicked context button type
    	var clickedBtnType = $(e.target).data('contextButton');

    	//userid that we clicked on
    	var clickedUserId = $(context).closest('div').data('userId');

    	if(clickedBtnType === 'btnToggleMute'){
    		toggleMuteHandler(clickedUserId);
    	} 
    	if(clickedBtnType === 'btnDeleteUser'){
    		deleteUserHandler(clickedUserId);
    	} 
    	if(clickedBtnType === 'btnAnswered'){
    		answerHandler(clickedUserId);
    	} 
	},
	scopes: 'video, img'
});


/*
*  Admin UI handlers
*/

//open the modal window with user credentials
var btnOpenRoomCredModal = document.getElementById('btnOpenRoomCredModal');
btnOpenRoomCredModal.onclick = function(e){
	e.preventDefault();
	modalUserCred.modal('show');
};

//create a room button handler(after filling user credentials)
btnCreateRoom.onclick = function(e){
	
	if(!validateRoomCredForm()) return;

	modalUserCred.modal('hide');

	//creating a room
	//sending room credentials to the server
	$.ajax({
	    url: baseWebRTCPath + 'server_scripts/create_room.php',
	    type: 'POST',
	    data: {password: roomPassword.val()},
	    success: function(resp) {
	    	var resp = JSON.parse(resp);
	    	roomName = resp.roomName;
	    	showFullLayout();
	    	setupWebRTC();
	    }
	});

};

//open the modal window with link to share
btnOpenModalShareLink.onclick = function(e){
	e.preventDefault();
	modalShareLink.modal('show');
	textShareLink.text(window.location.origin + window.location.pathname + "?room=" + roomName);
};

/*
*  WebRTC connection handlers
*/

//function for sharing part of the screen
connection.sharePartOfScreen = function(element){
	html2canvas(element, {
		onrendered: function(canvas){
			var screenshot = canvas.toDataURL('image/webp', 1);
			var dataToSend = {
				screenshot: screenshot,
				imageWebp: true
			};
			connection.send(dataToSend);
			connection.sharePartOfScreen(element);
		},
		grabMouse: false
	});
}

/*
*  MISC functions
*/

//disconnects user from our connection
//userIdToDelete - remote userId whom we want to delete
function deleteUserHandler(userIdToDelete){

	//disconnect user from admin connection
	connection.disconnectWith(userIdToDelete);

	//notify other peers that we disconnected a user
	connection.extra.lastDeletedUsedId = userIdToDelete;
	connection.updateExtraData();

}

//when admin answers a question
//answeredUserId - userId who we answered 
function answerHandler(answeredUserId){
	//notify other peers that we answered a question
	connection.extra.lastAnsweredUserId = answeredUserId;
	connection.updateExtraData();
	//update on our side
	updateContainerUserFaces();

	connection.extra.lastAnsweredUserId = '';
}

//toggles mute/unmute audio from admin side
//userIdToToggleMute - user id whose audio admin toggles
function toggleMuteHandler(userIdToToggleMute){
	connection.extra.lastToggledMuteUserId = userIdToToggleMute;
	//notify other peers that we toggled user's audio
	connection.updateExtraData();

	connection.extra.lastToggledMuteUserId = '';
}