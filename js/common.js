/*
*  Common variables for admin and user
*/

//common link for images
var baseWebRTCPath = window.location.href.substr(0, window.location.href.lastIndexOf('/') + 1) + 'tutorialspoint-webrtc/';
//webrtc panel
var webrtcPanel = $('#webrtcPanel');
//container for shared screen of admin(displayed on the user side)
var containerSharedPartOfScreenPreview = $("#containerSharedPartOfScreenPreview");
//modal window for user credentials(display name and password)
var modalUserCred = $("#modalRoomCredentials");
//display name input
var displayName = $("#displayName");
//room password input
var roomPassword = $("#roomPassword");
//create a room button(after filling user credentials)
var btnCreateRoom = document.getElementById('btnCreateRoom');
//global RTCMultiConnection object
var connection = new RTCMultiConnection();
//send message button
var btnSendMsg = document.getElementById('btnSendMsg');
//chat container(for text msgs and files)
var chatContainer = document.getElementById('chatContainer');
//share file button
var btnShareFile = document.getElementById('btnShareFile');
//input for text message(needed for emoji wysiwyg)
var inputTextMsg = $("#inputTextMsg");
//emoji init
$.emojiarea.path = baseWebRTCPath + 'js/libs/jquery-emojiarea-master/packs/basic/images';
inputTextMsg.emojiarea({button:"#btnAddSmile"});
//add smile button
var btnAddSmile = document.getElementById('btnAddSmile');
//wysiwyg content(text msg input + emojis)
var inputEmojiEditor = $(".emoji-wysiwyg-editor");
//container for user faces in the top part of the chat
var containerUserFaces = $("#containerUserFaces");
//container for bigger user video and the fullscreen button
var containerBigVideoAndFullscreen = $("#containerBigVideoAndFullscreen");
//container for bigger video
var containerBigVideo = $("#containerBigVideo");
//the current big video is being shown in the page
var currentBigVideo = {src: "", streamid: "", enabled: false};
//button fullscreen
var btnFullscreen = document.getElementById('btnFullscreen');
//last user states(enabledAudio and enabledVideo)
var lastUsersAVStates = [];
//whether we init slickjs for the 1st time(need for correct unslick() function)
var isFirstSlickInit = true;
//share video button
var btnShareVideo = document.getElementById('btnShareVideo');
//share audio button
var btnShareAudio = document.getElementById('btnShareAudio');
//mute/unmute local audio button
var btnToggleMute = document.getElementById('btnToggleMute');
//open the modal window with the link to share
var btnOpenModalShareLink = document.getElementById('btnOpenModalShareLink');

/*
*  Common handlers for admin and user
*/

/*
* UI handlers
*/

//send text message button handler
btnSendMsg.onclick = function(e){
	//if a message is empty then don't show it
	if(inputEmojiEditor.html().length === 0) return;

	var msg = displayName.val() + " > " + inputEmojiEditor.html();
	onMessageHandler(msg);
	connection.send(msg);
	inputEmojiEditor.html('');
}

//when clicking the "enter" button when writing in a chat
inputEmojiEditor.keydown(function(event){
	if(event.which === 13) btnSendMsg.dispatchEvent(new Event('click'));
});

//add smile button handler
btnAddSmile.onclick = function(e){
	e.preventDefault();
	//when changing the smile group prevent from redirect
	$(".tab_switch").on('click' ,function(e){ e.preventDefault(); });
};

//share file button handler
btnShareFile.onclick = function(e){
	e.preventDefault();
	var fileSelector = new FileSelector();
	fileSelector.selectSingleFile(function(file){
		connection.send(file);
	});
}

//share video button
btnShareVideo.onclick = function(event){
	event.preventDefault();
	connection.extra.enabledVideo = !connection.extra.enabledVideo;
	connection.extra.enabledAudio = false;
	//if we disabled our local video and still have our local video in the "big video" section then hide it
	if(connection.extra.enabledVideo === false && currentBigVideo.enabled && connection.streamEvents.selectFirst().streamid === currentBigVideo.streamid){
		currentBigVideo.enabled = false;
		//TODO: if dont want videos appear in the big video section after reshowing then probably need to flush the currentBigVideo var
	}

	//if we disable video then disable the audio stream
	if(!connection.extra.enabledVideo) connection.extra.isAudioMuted = true;

	//notify other peers that we enabled video
	connection.updateExtraData();
	//updating on our side
	updateContainerUserFaces();
};

//share audio button
btnShareAudio.onclick = function(event){
	event.preventDefault();
	connection.extra.enabledAudio = !connection.extra.enabledAudio;
	connection.extra.enabledVideo = false;
	//if we disable audio then disable the audio stream
	if(!connection.extra.enabledAudio) connection.extra.isAudioMuted = true;
	//notify other peers that we enabled audio and disabled video
	connection.updateExtraData();
	//updating on our side
	updateContainerUserFaces();
};

//mute/unmute audio
btnToggleMute.onclick = function(event){

	event.preventDefault();

	connection.extra.isAudioMuted = !connection.extra.isAudioMuted;

	updateContainerUserFaces();
};

//when clicking on the fullscreen button
btnFullscreen.onclick = function(event){
	event.preventDefault();
	var elem = document.getElementById(currentBigVideo.streamid);
	if (elem.requestFullscreen) {
		elem.requestFullscreen();
	} else if (elem.msRequestFullscreen) {
		elem.msRequestFullscreen();
	} else if (elem.mozRequestFullScreen) {
		elem.mozRequestFullScreen();
	} else if (elem.webkitRequestFullscreen) {
		elem.webkitRequestFullscreen();
	}
}

//when clicking on the video in the top menu move it to the middle and make bigger
containerUserFaces.on('click', 'video', function(){
	//save old video stream
	currentBigVideo.src = $(this).attr('src');
	currentBigVideo.streamid = $(this).attr('id');
	currentBigVideo.enabled = true;
	updateContainerUserFaces();
});

/*
* WebRTC handlers
*/

//on text message receive
connection.onmessage = onMessageHandler;

//on local/remote stream created
connection.onstream = function(event){
	//mute audio on local stream by default
	if(event.type === 'local'){
		connection.streamEvents.selectFirst().stream.mute('audio');
	}
};

//when other users connects to our session
connection.onopen = function(event){
	updateContainerUserFaces();
};

//when other peer changes his extra data
connection.onExtraDataUpdated = function(event){
	
	//check whether we have the last state for user who updated his extra data
	var isLastStateExists = false;
	var lastUserState;
	var lastUserStateIndex;
	lastUsersAVStates.forEach(function(el, i){
		if(el.userid === event.userid){
			isLastStateExists = true;
			lastUserState = el;
			lastUserStateIndex = i;
		} 
	});

	//if we don't have the last state then just save it
	if(!isLastStateExists){
		lastUsersAVStates.push(event);
		updateContainerUserFaces();
	} else {
		//if enabledVideo or enabledAudio or user raised a hand or lastAnsweredUserId changed then update top menu with user faces
		if(lastUserState.extra.enabledVideo != event.extra.enabledVideo || 
			lastUserState.extra.enabledAudio != event.extra.enabledAudio ||
			lastUserState.extra.raisedHand != event.extra.raisedHand){
			updateContainerUserFaces();
		}
		//if admin disconnected a user then we must also disconnect him(user faces will be updated in the ON CLOSE event)
		if(lastUserState.extra.lastDeletedUsedId != event.extra.lastDeletedUsedId){

			connection.disconnectWith(event.extra.lastDeletedUsedId);
			
			//TODO: if we were disconnected then redirect to home with a popup
			//if(connection.userid === event.extra.lastDeletedUsedId){
			//	window.location = window.location.origin + window.location.pathname;
			//}

		}
		//if we were answered a question then notify other users that we have no questions
		if(event.extra.lastAnsweredUserId === connection.userid){
			connection.extra.raisedHand = false;
			updateContainerUserFaces();
			connection.updateExtraData();
		}
		//if we were toggled mute/unmute then do it
		if(event.extra.lastToggledMuteUserId === connection.userid){
			connection.extra.isAudioMuted = !connection.extra.isAudioMuted;
			updateContainerUserFaces();
		}
		//update to the latest user state
		lastUsersAVStates.splice(lastUserStateIndex, 1);
		lastUsersAVStates.push(event);
	}

};

connection.onclose = function(){
	updateContainerUserFaces();
};

connection.onleave = function(){
	updateContainerUserFaces();
};

//NOTE: we need mute/unmute handlers because local stream in other case is not muted and there is an echo issue
connection.onmute = function(e){
	//console.log('ON MUTE');
	//console.log(e);
};

connection.onunmute = function(e){
	//console.log('ON UNMUTE');
	//console.log(e);
};

/*
*  MISC functions
*/

//changing page layout to show webrtc UI
function showFullLayout(){
	//hide the "start a chat button"
	$("#btnOpenRoomCredModal").hide();
	//show webrtc panel
	$("#cc").layout('expand', 'east');
	if(role === 'user'){
		//hide tools and their properties
		$("#cc").layout('collapse', 'west');
		$(".subProperties, .db-nav").hide();
		//delete admin canvas
		$(".canvas-container").remove();
		//show shared screen
		containerSharedPartOfScreenPreview.show();
	}
}

//get URL parameter by name
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

//setup webrtc connection after room creation or entering credentials
function setupWebRTC(){

	//custom signaling server
	connection.socketURL = 'https://ryzhak.com:9001/';
	//connection.socketURL = 'https://localhost:9001/';
    connection.socketMessageEvent = 'webrtcTPsocketMsgEvent';

    //signaling server for testing puposes
    //connection.socketURL = 'https://rtcmulticonnection.herokuapp.com:443/';

	connection.enableFileSharing = true; 
	connection.filesContainer = chatContainer;

	connection.sdpConstraints.mandatory = {
        OfferToReceiveAudio: true,
        OfferToReceiveVideo: true
    };

    connection.session = {
        audio: true,
        video: true,
        data: true
    };

    //adding display name to the conenction
    connection.extra.displayName = displayName.val();
    //local audio and video are disabled by default
	connection.extra.enabledVideo = false;
	connection.extra.enabledAudio = false;
	connection.extra.isAudioMuted = true;
	//last deleted user by admin
	connection.extra.lastDeletedUsedId = '';
	//whether a user raised a hand
	connection.extra.raisedHand = false;
	//last answered user id by admin
	connection.extra.lastAnsweredUserId = '';
	//last muted/unmuted user id by admin
	connection.extra.lastToggledMuteUserId = '';

    if(role === 'admin'){
    	//creating a room
    	connection.open(roomName);
    	//sharing part of screen
    	connection.sharePartOfScreen(partOfScreenToBeShared);
    }
    if(role === 'user'){
    	//join an existing room for the user side
    	connection.join(roomNameFromUrl);
    }

    updateContainerUserFaces();
	
}

//adds text message to the chat
function onMessageHandler(event){

	//if user receives the image from admin screen that he is sharing
	if(typeof event === 'object'){
		if(event.data.hasOwnProperty('imageWebp')){
			sharedPartOfScreenPreview.src = event.data.screenshot;
			return;
		}
	}

	//TODO: change the chat direction from top to bottom
	var div = document.createElement('div');
    div.innerHTML = event.data || event;
    //chatContainer.insertBefore(div, chatContainer.firstChild);
    chatContainer.appendChild(div);

    //scroll chat to the bottom
    chatContainer.parentNode.scrollTop = chatContainer.lastChild.offsetTop;
    
}

//updates user images/video at the top of the webrtc panel
function updateContainerUserFaces(){
	
	//clear all
	if(!isFirstSlickInit) containerUserFaces.slick('unslick');
	containerUserFaces.html('');
	btnToggleMute.style.display = 'none';
	containerBigVideo.html('');
	containerBigVideoAndFullscreen.hide();

	//check whether we need to add the raise-hand class to an element
	var raisedHandClass = (connection.extra.raisedHand) ? 'raised-hand' : '' ;

	//if we have our local video enabled
	if(connection.extra.enabledVideo){
		var localStream = connection.streamEvents.selectFirst();
		//adding the video tag
		var videoTagHTML = '<div class="user-face ' + raisedHandClass + '" data-user-id=' + connection.userid + '><video src= ' + localStream.blobURL + ' id=' + localStream.streamid + ' muted></video></div>';
		//if we need to display this video in the "big video" section
		if(currentBigVideo.enabled && (currentBigVideo.streamid === localStream.streamid)){
			containerBigVideo.html(videoTagHTML);
			//show the big video
			containerBigVideoAndFullscreen.show();
		} else {
			//isnert the video in the top menu with other user faces
			containerUserFaces.append(videoTagHTML);
		}
		//playing the video
		setTimeout(function(){document.getElementById(localStream.streamid).play();}, 100);
	} else {
		//we have audio enabled then set image path to anonym with headphones
		if(connection.extra.enabledAudio){
			var imgPath = baseWebRTCPath + "images/anonym_with_headphones.png";
		} else {
			//audio and video are disabled
			var imgPath = baseWebRTCPath + "images/anonym.jpg";
		}
		//insert img
		containerUserFaces.append('<div class="user-face ' + raisedHandClass + '" data-user-id=' + connection.userid + '><img title=' + connection.extra.displayName + ' src="' + imgPath + '" /></div>');
	}

	//showing the mute/unmute local audio button
	if(connection.extra.enabledVideo || connection.extra.enabledAudio) btnToggleMute.style.display = 'inline-block';
	updateLocalAudio();

	//for each connected users
	connection.getAllParticipants().forEach(function(userid){

		var remotePeer = connection.peers[userid];
		
		raisedHandClass = (remotePeer.extra.raisedHand) ? 'raised-hand' : '' ;

		//if a remote user has video enabled and has a stream
		if(remotePeer.extra.enabledVideo && remotePeer.streams.length != 0){
			var remoteStreamId = remotePeer.streams[0].streamid;
			var remoteStream = connection.streamEvents[remoteStreamId];
			//adding the video tag
			var videoTagHTML = '<div class="user-face ' + raisedHandClass + '" data-user-id=' + userid + '><video src= ' + remoteStream.blobURL + ' id=' + remoteStream.streamid + '></video></div>';
			//if we need to display this video in the "big video" section
			if(currentBigVideo.enabled && (remoteStream.streamid === currentBigVideo.streamid)){
				containerBigVideo.html(videoTagHTML);
				//show the big video
				containerBigVideoAndFullscreen.show();
			} else {
				//display video in the top menu
				containerUserFaces.append(videoTagHTML);
			}
			//playing the video
			setTimeout(function(){document.getElementById(remoteStream.streamid).play();}, 100);
		} else {
			//if remote peer has audio enabled
			if(remotePeer.extra.enabledAudio){
				imgPath = baseWebRTCPath + "images/anonym_with_headphones.png";
			} else {
				//audio and video are disabled
				imgPath = baseWebRTCPath + "images/anonym.jpg";
			}
			//insert img
			containerUserFaces.append('<div class="user-face ' + raisedHandClass + '" data-user-id=' + userid + '><img title=' + remotePeer.extra.displayName + ' src="' + imgPath + '" /></div>');
		}
		
	});

	//activate slick slider
	containerUserFaces.slick({
	  infinite: true,
	  slidesToShow: 3,
	  slidesToScroll: 1
	});
	isFirstSlickInit = false;

}

//mute/unmute local audio stream and its icon
function updateLocalAudio(){
	if(typeof connection.streamEvents.selectFirst() != 'undefined'){
		var localStream = connection.streamEvents.selectFirst().stream;
		if(connection.extra.isAudioMuted){
			localStream.mute('audio');
			btnToggleMute.innerHTML = '<span class="glyphicon glyphicon-volume-off" aria-hidden="true"></span>';
		} else {
			localStream.unmute('audio');
			btnToggleMute.innerHTML = '<span class="glyphicon glyphicon-volume-up" aria-hidden="true"></span>';
		}
	}
}

//validate creating/entering a room form
//status - response status when user tries a password
function validateRoomCredForm(status){
	var res = true;
	var status = status || '';

	//clear all errors
	displayName.parent('.form-group').removeClass('has-error');
	roomPassword.parent('.form-group').removeClass('has-error');

	//display name can not be empty
	if(displayName.val().length === 0){
		displayName.parent('.form-group').addClass('has-error');
		res = false;
	}

	//if incorrect password
	if(status === 'error'){
		roomPassword.parent('.form-group').addClass('has-error');
		res = false;
	}

	return res;

}

/*
*  Loading the script depending on a user role
*/
var roomNameFromUrl = getParameterByName('room'); 
var role = roomNameFromUrl ? 'user' : 'admin' ;

if(role === 'admin'){
	//load context menu script
	$.getScript(baseWebRTCPath + 'bower_components/bootstrap-contextmenu/bootstrap-contextmenu.js');
	$.getScript(baseWebRTCPath +'js/admin.js');
} else {
	$.getScript(baseWebRTCPath + 'js/user.js');
}
