/*
*  User variables
*/

//where to show admin's shared screen
var sharedPartOfScreenPreview = document.getElementById('sharedPartOfScreenPreview');
//raise hand button
var btnRaiseHand = document.getElementById('btnRaiseHand');

/*
*  Change the page layout
*/

//changing label and button text for modal window with user credentials
$("#modalRoomCredentialsLabel").text("Enter a room");
btnCreateRoom.textContent = "Enter";
//showing a modal window with user credentials
modalUserCred.modal('show');
//hide open the modal window with the link to share button
btnOpenModalShareLink.style.display = 'none';
//show raise hand button
btnRaiseHand.style.display = 'inline-block';

/*
*  WebRTC conenction handlers
*/

//enter a room button handler(after filling user credentials)
btnCreateRoom.onclick = function(e){
	//entering a room
	//sending room credentials to the server
	$.ajax({
	    url: baseWebRTCPath + 'server_scripts/enter_room.php',
	    type: 'POST',
	    data: {room: roomNameFromUrl, password: roomPassword.val()},
	    success: function(resp) {
	    	var resp = JSON.parse(resp);

	    	if(!validateRoomCredForm(resp.status)) return;
	    	
	    	modalUserCred.modal('hide');
	    	showFullLayout();
	    	setupWebRTC();
	    	
	    }
	});
};

//when user clicks the raise hand button
btnRaiseHand.onclick = function(e){
	e.preventDefault();
	connection.extra.raisedHand = true;
	//notify other users that we've just raised a hand
	connection.updateExtraData();
	updateContainerUserFaces();
}